import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../widgets/custom_buttons.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF4415EE),
      body: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'TOP RATED FLUTTER\nDEVELOPMENT\nCOMPANY',
                  style: TextStyle(fontSize: 36, color: Colors.white),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: FittedBox(
                  child: Text(
                    'FlutterDevs specializes in creating cost-effective and efficient\napplications with our perfectly crafted, creative and leading-\nedge flutter app development solutions for customers all\naround the globe.',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
              customButton3('Request a quote'.toUpperCase(), () {}),
            ],
          )),
           Expanded(
              child: Image.asset('assets/images/intro.gif'))
        ],
      ),
    );
  }
}

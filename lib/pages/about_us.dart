import 'package:flutter/material.dart';

import '../widgets/about_us_text_span.dart';
import '../widgets/custom_widgets.dart';

class AboutUs extends StatefulWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  State<AboutUs> createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF9F8FF),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: AboutUsTextSpan(fontSize: 30),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: FittedBox(
                  child: Text(
                    'We are a Top-Rated Flutter Development\nCompany around the Globe.',
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: FittedBox(
                  child: Text(
                    'FlutterDevs is a protruding flutter app development company with an extensive in-\nhouse team of 30+ seasoned professionals who know exactly what you need to\n strengthen your business across various dimensions. With more than 10+ years of\nexperience in mobile applications, we know your needs very well.',
                    style: TextStyle(fontSize: 25, color: Colors.black54),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  myContainer('10+', 'Years of Mobility\nExperience'),
                  myContainer('30+', 'Flutter\nExperts'),
                  myContainer('500+', 'Successful\nApplications')
                ],
              )
              // customButton3('Request a quote'.toUpperCase(), () {}),
            ],
          )),
          Expanded(
              child: Tooltip(
            message: 'We are a Top-Rated Flutter Development Company',
            child: Image.asset('assets/images/about us.png'),
          ))
        ],
      ),
    );
  }
}

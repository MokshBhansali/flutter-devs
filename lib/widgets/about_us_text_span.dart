import 'package:flutter/material.dart';

import '../res/colors.dart';

class AboutUsTextSpan extends StatelessWidget {
  final double fontSize;
  const AboutUsTextSpan({Key? key, required this.fontSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: 'About ',   //first part
        style: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.w900,
            color: Colors.grey[800]),
        children: const <TextSpan>[
          TextSpan(
              text: 'Us',  //second part
              style:
                  TextStyle( color: primaryColor)),
        ],
      ),
    );
  }
}

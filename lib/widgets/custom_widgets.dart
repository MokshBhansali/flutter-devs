import 'package:flutter/cupertino.dart';
import 'package:flutter_devs/res/colors.dart';

Container myContainer(String text1, String text2) {
  return Container(
    height: 100,
    width: 160,
    padding: const EdgeInsets.all(8),
    decoration: BoxDecoration(
        color: const Color(0xFFE7E2FE),
        borderRadius: BorderRadius.circular(10)),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text1,
              style: const TextStyle(
                color: primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              )),
          Text(text2,
              style: const TextStyle(
                color: blackColor,
                fontSize: 16,
                fontWeight: FontWeight.w800,
              )),
        ]),
  );
}

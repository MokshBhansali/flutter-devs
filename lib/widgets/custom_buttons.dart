import 'package:flutter/material.dart';

customButton(String text, void Function()? onPressed) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: TextButton(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: const BorderSide(color: Color(0xFFF25767)))),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.white;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return const Color(0xFFF25767);
        }),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.red;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return Colors.white;
        }),
      ),
      onPressed: onPressed,
      child: Text(text),
    ),
  );
}

customButton3(String text, void Function()? onPressed) {
  return Container(
    padding: const EdgeInsets.all(8.0),
    width: 200,
    height: 60,
    child: TextButton(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: const BorderSide(color: Color(0xFFF25767)))),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.white;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return const Color(0xFFF25767);
        }),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.red;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return Colors.white;
        }),
      ),
      onPressed: onPressed,
      child: Text(text),
    ),
  );
}

customButton1(String text, void Function()? onPressed) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: TextButton(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: const BorderSide(color: Color(0xFFF25767)))),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) {
            return const Color(0xFF4429F9);
          }
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return const Color(0xFFF25767);
        }),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.white;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return Colors.white;
        }),
      ),
      onPressed: onPressed,
      child: Text(text),
    ),
  );
}

customButton2(void Function()? onPressed) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: TextButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) {
            return const Color(0xFFFF8A73);
          }
          if (states.contains(MaterialState.pressed)) {
            return const Color(0xFFFF8A73);
          }
          return const Color(0xFF3621C5);
        }),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (states.contains(MaterialState.hovered)) return Colors.white;
          if (states.contains(MaterialState.pressed)) return Colors.blue;
          return Colors.white;
        }),
      ),
      onPressed: onPressed,
      child: const Icon(Icons.menu),
    ),
  );
}

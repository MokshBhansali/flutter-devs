import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF4429F9);
const Color blackColor = Colors.black;
const Color whiteColor = Colors.white;
const Color redColor = Colors.red;

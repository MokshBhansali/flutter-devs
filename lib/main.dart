import 'package:flutter/material.dart';

import 'pages/about_us.dart';
import 'pages/empower.dart';
import 'pages/intro_page.dart';
import 'pages/partnership.dart';
import 'widgets/custom_buttons.dart';
import 'widgets/scroll_control.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Devs',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: const Color(0xFF4429F9),
          // secondary: const Color(0xFFFFC107),
        ),
      ),
      scrollBehavior: AppScrollBehavior(),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButton: FloatingActionButton(onPressed: (){}),
        appBar: AppBar(
          backgroundColor: const Color(0xFF4415EE),
          elevation: 0,
          leading: Image.asset(
              'assets/images/logo.png'),
          // title: Text(widget.title),
          actions: [
            customButton1('Connect', () {}),
            customButton('Hire Freelancers', () {}),
            customButton2(() {})
          ],
        ),
        body: PageView(
          scrollDirection: Axis.vertical,
          children: const <Widget>[
            IntroPage(),
            AboutUs(),
            Empower(),
            Partnership()
            // Container(
            //   color: Colors.deepPurple[200],
            // ),
          ],
        ));
  }
}
